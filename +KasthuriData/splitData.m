function [ idxTrain, idxVal, idxTest ] = splitData( borderCoM, ...
    bboxTrain, bboxVal, bboxTest )
%SPLITDATA Split data into the specified bboxes.
% INPUT borderCoM: [Nx3] int
%           Global border centroids.
%       bboxTrain: [3x2] int
%           Bounding box for training interfaces.
%       bboxVal: [3x2] int
%           Bounding box for validation interfaces. Can be empty.
%       bboxTest: [3x2] int
%           Bounding box for test interfaces. Can be empty.
% OUTPUT idxTrain: [Nx1] logical
%           Logical indices w.r.t. borderCoM for the training interfaces.
%        idxVal: [Nx2] logical
%           Logical indices w.r.t. borderCoM for the validation interfaces.
%        idxTest: [Nx2] logical
%           Logical indices w.r.t. borderCoM for the test interfaces.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

idxTrain = SynEM.Util.isInBBox(borderCoM, bboxTrain);
if exist('bboxVal', 'var') && ~isempty(bboxVal)
    idxVal = SynEM.Util.isInBBox(borderCoM, bboxVal);
else
    idxVal = [];
end
if exist('bboxTest', 'var') && ~isempty(bboxTest)
    idxTest = SynEM.Util.isInBBox(borderCoM, bboxTest);
else
    idxTest = [];
end


end

