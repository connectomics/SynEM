function [ skel ] = downSample( skel,treeIdx,downsamplefactor )
% Downsample a Tree.
% INPUT treeIndex: Index (not id!) of tree in skel.(Default=all trees)
%       downsamplefactor: The factor by which degree 2 nodes would be
%       downsampled
%OUTPUT skel: Downsampled skeleton
if ~exist('treeIndices','var') || isempty(treeIdx)
    treeIdx = 1:skel.numTrees();
end

for tr = treeIdx
   secondDegreeNodes =find(cell2mat(skel.calculateNodeDegree(tr))==2);
   nodesToDelete = secondDegreeNodes(find(arrayfun(@mod,1:size(secondDegreeNodes),repmat(downsamplefactor,[1,size(secondDegreeNodes)]))));
   skel = skel.deleteNodes(tr,nodesToDelete,true);
end

end

