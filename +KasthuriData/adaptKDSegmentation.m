function adaptKDSegmentation( kd_in, kd_out, bbox, cubesize)
%ADAPTKDSEGMENTATION Wrapper to adapt the segmentation for a knossos
%dataset.
% INPUT kd_in: KnossosDataset object or struct
%           The original segmentation KnossosDataset or parameter struct.
%       kd_out: KnossosDataset object or struct
%           The output segmentation KnossosDataset or parameter struct.
%       bbox: (Optional) [3x2] int
%           The segmentation bounding box.
%       cubesize: (Optional) [1x3] int
%           The size of cubes for which the adaptation is run.
%           (Default: kd_out.cubesize(1:3))
% see also KasthuriData.adaptSegmentation.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if isstruct(kd_in)
    kd_in = KnossosDataset(kd_in);
end
if isstruct(kd_out)
    kd_out = KnossosDataset(kd_out);
end

if ~exist('bbox', 'var') || isempty(bbox)
    bbox = kd_in.getBbox();
end
if ~exist('cubesize', 'var') || isempty(cubesize)
    cubesize = kd_out.cubesize(1:3);
% elseif any(mod(cubesize, kd_out.cubesize(1:3)))
%     warning('The cubesize is not a multiple of the output cubesize.');
end
cubesize = cubesize(:);

for x = bbox(1,1):cubesize(1):bbox(1,2)
    for y = bbox(2,1):cubesize(2):bbox(2,2)
        for z = bbox(3,1):cubesize(3):bbox(3,2)
            this_bbox = [[x; y; z], [x; y; z] + cubesize - 1];
            this_bbox(:,2) = min(this_bbox(:,2), bbox(:,2));
            seg = kd_in.readRoi(this_bbox);
            seg = KasthuriData.adaptSegmentation(seg);
            kd_out.writeRoi(this_bbox, seg);
        end
    end
end

end

