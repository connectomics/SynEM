function area = physicalBorderArea(borders, scale, cubeSize, minArea)
%PHYSICALBORDERAREA Calculate the border area in physical units.
% INPUT borders: Struct array containing the field 'PixelIdxList' of linear
%           pixels of each border w.r.t. to a cube of size cubeSize.
%       scale: (Optional) [1x3] array of double containing the voxel size in
%           the desired physical unit.
%           (Default: [11.24, 11.24, 28])
%       cubeSize: (Optional) [1x3] array of integer specifying the size of the
%           cube to which the linear indices in boders refer.
%           (Default: [512, 512, 256])
%       minArea: (Optional) Minimal area for a contact surface in um^2.
%           (Default: 5e-4 - 4 voxels in x-y-plane)
% OUTPUT area: [Nx1] array of double where N = length(borders) specifying the
%           physical area for each border in borders in um^2.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('scale','var') || isempty(scale)
    scale = [11.24, 11.24, 28];
end
if ~exist('cubeSize','var') || isempty(cubeSize)
    cubeSize = [512, 512, 256];
end
if ~exist('minArea','var') || isempty(minArea)
    minArea = 5e-4;
end

area = zeros(length(borders),1);
for i = 1:length(borders)
    if isrow(borders(i).PixelIdxList)  
    [x,y,z] = ind2sub(cubeSize,borders(i).PixelIdxList');
    else
    [x,y,z] = ind2sub(cubeSize,borders(i).PixelIdxList);
    end
    nodes = [x,y,z];
    nodes = bsxfun(@times,nodes,scale);
    area(i) = calculateContactArea(nodes,minArea);
end
end

function area = calculateContactArea(nodes,minimumDistance)
% Assumes nodes passed are given in nm, minimum distance in microns^2
% Need to set lower threshold if not enough points are specified?
% Author: Manuel Berning <manuel.berning@brain.mpg.de>
if size(nodes,1) > 3
    [~, score] = pca(nodes);
    try
        k = convhull(score(:,1),score(:,2));
        area = polyarea(score(k,1),score(k,2))/1e6;
    catch
        %collinear points
        area = minimumDistance;
    end
else
    % Lower cutoff
    area = minimumDistance;
end

end

