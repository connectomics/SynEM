function getSynapticBorders(p, kd_sj, mode, varargin)
%GETSYNAPTICBORDERS Get the synaptic segmentation borders.
% INPUT p: struct
%           Segmentation parameter struct.
%       kd_sj: KnossosDataset or struct
%           Object or struct for synapse knossos dataset.
%       mode: string
%           The calculation mode. Options are
%           'overlap_at': Each border is synaptic that has more synaptic
%               pixels than a specified threshold.
%               varargin{1} in this case is the minimal number of pixels
%               that need to be synaptic in order to accept the border as
%               synaptic. The default value is 1.
%           'overlap_rt': Each border is synaptic that has a higher fraction
%               synaptic pixels than a specified threshold.
%               varargin{1} in this case is the minimal number of pixels
%               that need to be synaptic in order to accept the border as
%               synaptic. The default value is 0.5.
%           'overlap_max': For each synapse only the border with the
%               maximal overlap is labeled synaptic.
%               varargin{1} in this case can be as in overlap_rt which is
%               in addition applied to the borders with maximal overlap.
%               Default is 0.25
%
% NOTE The outputs are directly saved to the local cubes. See below for the
%      output names.
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if isstruct(kd_sj)
    kd_sj = KnossosDataset(kd_sj);
end

switch mode
    case 'overlap_at'
        saveName = 'synapseGT_at.mat';
    case 'overlap_rt'
        saveName = 'synapseGT_rt.mat';
    case 'overlap_max'
        saveName = 'synapseGT_max.mat';
    otherwise
        error('Unknown mode %s.', mode);
end

for i = 1:numel(p.local)
    Util.log('Processing cube %d/%d.', i, numel(p.local));
    
    % get cube borders
    m = load(p.local(i).borderFile);
    borders = m.borders;
    
    % next iteration if there are no borders in this cube
    if isempty(borders)
        synId = [];
        Util.save([p.local(i).saveFolder saveName], synId);
        continue
    end
    
    borderPixels = {borders.PixelIdxList};

    % get cube synapse segmentation
    synSeg = kd_sj.readRoi(p.local(i).bboxSmall);
    
    switch mode
        
        case 'overlap_at'
            
            if ~isempty(varargin)
                sjT = varargin{1};
            else
                sjT = 1;
            end
            assert(sjT > 0, 'The threshold must be > 0.');

            % get synapse lables for border pixels
            borderSynPixels = cellfun(@(x)synSeg(x), borderPixels, ...
                'uni', 0);
            borderSynPixels = cellfun(@(x)x(x > 0), borderSynPixels, ...
                'uni', 0);
            l = cellfun(@length, borderSynPixels);

            % label borders with more than sjT synapse pixels as synaptic
            synId = zeros(length(borders), 1, 'uint32');
            synId(l >= sjT) = cellfun(@getId, borderSynPixels(l >= sjT));

            % save result
            Util.save([p.local(i).saveFolder saveName], ...
                synId, sjT, mode);
            
            
        case 'overlap_rt'
            
            if ~isempty(varargin)
                sjT = varargin{1};
            else
                sjT = 0.5;
            end
            assert(sjT > 0 && sjT <= 1, 'The threshold must be in (0, 1]');

            % get synapse lables for border pixels
            borderSynPixels = cellfun(@(x)synSeg(x), borderPixels, ...
                'uni', 0);
            synFract = cellfun(@(x)sum(x > 0)/length(x), borderSynPixels);
            borderSynPixels = cellfun(@(x)x(x > 0), borderSynPixels, ...
                'uni', 0);

            % label borders with more than sjT synapse pixels as synaptic
            synId = zeros(length(borders), 1, 'uint32');
            synId(synFract >= sjT) = cellfun(@getId, ...
                borderSynPixels(synFract >= sjT));

            % save result
            Util.save([p.local(i).saveFolder saveName], ...
                synId, sjT, mode);
            
            
        case 'overlap_max'
            
            if ~isempty(varargin)
                sjT = varargin{1};
            else
                sjT = 0.25;
            end
            assert(sjT > 0 && sjT <= 1, 'The threshold must be in (0, 1]');
            
            % run connected components for synapses (because the
            % segmentation does not contain different ids for each synapse)
            synSeg = bwlabeln(synSeg > 0);
            
            % get the unique synapses for this cube
            ids = setdiff(synSeg, 0);
            
            % get the number of pixels each border has for the different
            % synapses
            borderSynPixels = cellfun(@(x)synSeg(x), borderPixels(:), ...
                'uni', 0);
            synFract = cellfun(@(x)sum(x > 0)/length(x), borderSynPixels);
            idCount = cell2mat(cellfun(@(x)idCounts(x, ids), ...
                borderSynPixels, 'uni', 0));
            
            % delete the ids that do not overlap with any border
            noBorder = ~any(idCount, 1);
            ids = ids(~noBorder);
            idCount = idCount(:, ~noBorder);
            
            % assign the border with the most pixels to the respective
            % synapse id
            [~, synIdx] = max(idCount, [], 1);
            
            synFractIdx = synFract(synIdx) >= sjT;
            
            synId = zeros(length(borders), 1, 'uint32');
            synId(synIdx(synFractIdx)) = ids(synFractIdx);
            
            % save result
            Util.save([p.local(i).saveFolder saveName], synId, ...
                sjT, mode);
    end
end

end

function id = getId(synLabels)
%Get the id for the synaptic labels collected by one single interface.
[c, nc] = Util.ucounts(synLabels);
if length(c) > 1
    warning('Border assigned to multiple ids.')
    [~,idx] = max(nc);
    id = c(idx);
else
    id = c;
end
end

function counts = idCounts(x, ids)
%The number of pixels having the reference ids. ids must be sorted.
[c, nc] = Util.ucounts(x);
idx = ismember(c, ids);
c(~idx) = [];
nc(~idx) = [];
counts = zeros(1, length(ids));
counts(ismember(ids, c)) = nc;
end
