function [synCenters] = getSynCenterFromTPA( skel,tr,synNodeIds)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    synCenterLogical=skel.reachableNodes(tr,synNodeIds,1,'exact_excl');
    synNodeIdsLogical=false(size(synCenterLogical));
    synNodeIdsLogical(synNodeIds)=true;
    debugMatrix=cat(2,synCenterLogical,synNodeIdsLogical);
    synCenters=find(synCenterLogical);
    assert(size(synCenters,1)==size(synNodeIds,1),...
        'The size of nodeIds and the synapse center does not match');
end

