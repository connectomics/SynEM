function skel = addNodesAsTrees(skel, nodes, name, comment)
%Add the specified nodes each in a single tree.
% INPUT nodes: [Nx3] array of integer containing the nodes.
%       name: (Optional) string or [Nx1] cell of strings
%           Name for each tree. If name is a string then it will get the
%           tree thingID as suffix. If name is a cell array then a name
%           must be provied for each input node. You can add a %d in the
%           cell array names to include the thingId in the name.
%           (Default: Naming convention from addTree).
%       comment: [Nx1] cell
%           Cell array of the same length size(nodes, 1) containing a
%           comment for each node.
% OUTPUT skel: The updated Skeleton object.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

for i = 1:size(nodes,1)
    if ~exist('name','var') || isempty(name)
        currName = [];
    elseif iscell(name)
        if strfind(name{i}, '%')
            if isempty(skel.thingIDs)
                id = 1;
            else
                id = max(skel.thingIDs) + 1;
            end
            currName = sprintf(name{i}, id);
        else
            currName = name{i};
        end
    else
        if isempty(skel.thingIDs)
            id = 1;
        else
            id = max(skel.thingIDs) + 1;
        end
        currName = [name,sprintf('%03d',id)];
    end
    skel = skel.addTree(currName, nodes(i,:));
    if exist('comment', 'var') && ~isempty(comment)
        skel.nodesAsStruct{end}.comment = comment{i};
    end
end
end