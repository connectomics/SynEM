function [label, group, exclList] = getDirectionLabels( p, skel, edges, ...
    borderCoM, exclInt )
%GETDIRECTIONLABELS Get the direction labels.
% INPUT p: struct or [Nx1] cell array
%           Segmentation parameter struct for the kasthuri data used to get
%           the segment ids of the skeleton nodes. Alternatively segIds can
%           directly be specified as input instead of p.
%           (see also Skeleton.getSegmentIdsOfSkel)
%       skel: skeleton object
%           The SynapseDirectionLabel.nml skeleton. Each tree with 3 nodes
%           is considered a valid tree with the first node pointing to a
%           synapse com, the second node to the corresponding presynaptic
%           and the third node to the postsynaptic process.
%       edges: [Nx2] int
%           Kasthuri data global edge list.
%       borderCoM: [Nx3] int
%           Global border com list.
%       exclInt: (Optional) [Nx1] logical or int
%           Linear or logical indices for the trees in skel for which the
%           labeled interface will be put on an exclusion list.
%           (Default: no exclusion)
% OUTPUT label: [Nx1] int
%           Direction label for each edge in edges. Labels are:
%           '0': non-synaptic
%           '1': synaptic with direction as in edges (i.e. first id for the
%               corresponding edge is presynaptic)
%           '2': synaptic with direction opposite to edges (i.e. second id
%               for the corresponding edge is presynaptic).
%        group: [Nx1] int
%           Grouping for the synapses, i.e. all interfaces belonging to the
%           same synapse get the same id.
%        exclList: [Nx1] int
%           Linear indices of interfaces that should be excluded.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% get segment ids
if isstruct(p)
    segIds = Skeleton.getSegmentIdsOfSkel(p, skel);
else
    segIds = p;
end

% prepare exclusion list
if ~exist('exclInt', 'var') || isempty(exclInt)
    exclInt = false(skel.numTrees(), 1);
end
if ~islogical(exclInt)
    tmp = false(skel.numTrees(), 1);
    tmp(exclInt) = true;
    exclInt = tmp;
else
    assert(length(exclInt) == skel.numTrees());
end

% get synapse coms
synCoMs = cell2mat(cellfun(@(x)x(1,1:3), skel.nodes, 'uni', 0));

% assert that the middle node always has two neighbors
nodeDegree = skel.calculateNodeDegree();
nodeDegree(cellfun(@isempty,nodeDegree)) = [];
nodeDegree = cellfun(@(x)x(2), nodeDegree);
assert(all(nodeDegree == 2));

% only keep trees with exactly 3 nodes
toDel = cellfun(@length, segIds) ~= 3;
segIds(toDel) = [];
synCoMs(toDel,:) = [];
exclInt(toDel) = [];

% delete trees outside of bbox
toDel = cellfun(@(x)any(x == -1), segIds);
segIds(toDel) = [];
synCoMs(toDel,:) = [];
exclInt(toDel) = [];

% check for non-zero ids for pre- and postsyn
segIds = cell2mat(segIds);
segIds = reshape(segIds, 3, [])';
segIds = segIds(:,2:3);
assert(all(segIds(:) > 0))

% find the possible borders for each synapse
idx = Graph.findEdges(edges, segIds);
idx_excl = idx(exclInt);
synCoMs_excl = synCoMs(exclInt,:);
idx = idx(~exclInt);
synCoMs = synCoMs(~exclInt,:);
segIds = segIds(~exclInt,:);

% get direction for each potential synapse interface
dir = idx;
for i = 1:length(dir)
    for j = 1:length(idx{i})
        dir{i}(j) = getSynDir(edges(idx{i}(j),:), segIds(i,:));
    end
end
assert(~any(cell2mat(dir) == -1)); % assert nothing went wrong

% automatically accept all synapses with exactly one edge
label = zeros(size(edges, 1), 1, 'uint8');
group = zeros(size(edges, 1), 1, 'uint32');
l = cellfun(@length, idx);
label(cell2mat(idx(l == 1))) = cell2mat(dir(l == 1));
group(cell2mat(idx(l == 1))) = 1:sum(l == 1);

% delete the already labeled synapses
dir(l == 1) = [];
idx(l == 1) = [];
synCoMs(l == 1,:) = [];

% get distance to com in nm
voxelSize = [8 8 40];
synCoMs = bsxfun(@times, double(synCoMs), voxelSize);
borderCoM = bsxfun(@times, double(borderCoM), voxelSize);
d = idx;
for i = 1:length(d)
    d{i} = pdist2(synCoMs(i,:), borderCoM(idx{i},:));
end

% labels those with less than 750 nm distance to syn com as synaptic as well
curGroup = max(group(:)) + 1;
dT = 750;
for i = 1:length(d)
    this_idx = d{i} < dT;
    label(idx{i}(this_idx)) = dir{i}(this_idx);
    group(idx{i}(this_idx)) = curGroup;
    curGroup = curGroup + 1;
end
assert(isequal(label > 0, group > 0));

% get the interfaces for the exclusion list
if nargout > 2
    exclList = false(size(edges, 1), 1);
    
    % directly label all those that mark exactly one edge (as above)
    l = cellfun(@length, idx_excl);
    exclList(cell2mat(idx_excl(l == 1))) = true;
    idx_excl(l == 1) = [];
    synCoMs_excl(l == 1,:) = [];
    
    % for the other mark only those within 750 nm (as above)
    d = idx_excl;
    synCoMs_excl = bsxfun(@times, double(synCoMs_excl), voxelSize);
    for i = 1:length(d)
        d{i} = pdist2(synCoMs_excl(i,:), borderCoM(idx_excl{i},:));
    end
    for i = 1:length(d)
        this_idx = d{i} < dT;
        exclList(idx_excl{i}(this_idx)) = true;
    end
end
end

function dirLabel = getSynDir(edge, synIds)
if edge(1,1) == synIds(1,1)
    dirLabel = 1;
elseif edge(1,2) == synIds(1,1)
    dirLabel = 2;
else
    % something went wrong
    dirLabel = -1;
end
end

