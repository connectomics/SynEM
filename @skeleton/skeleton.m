classdef skeleton
    %Skeleton Class representing a Skeleton tracing.

    properties
        nodes = {};
        nodesAsStruct = {};
        nodesNumDataAll = {};
        nodesTable = {};
        edges = {};
        parameters;
        thingIDs;
        names = {};
        colors = {};
        branchpoints;
        largestID = 0;
        scale;
        nodeOffset;
        verbose = true;
        filename = ''; %the filename without .nml
    end

    methods
        function obj = skeleton(filename, justCloneParameters, nodeOffset, verbose, options)
            % Construct Skeleton class from nml file.
            % obj = Skeleton() returns an empty Skeleton object.
            %
            % obj = Skeleton(filename, justcloneparameters, nodeOffset)
            % requires the following inputs.
            % INPUT filename: Full path (including '.nml') to nml file.
            %       justCloneParameters: Only parameter struct is written to
            %                           skel object. (Default: false)
            %       nodeOffset: (Optional) Double specifying an offset to
            %           the x, y and z coordinates of all nodes.
            %           (Default: 1).
            %       verbose: (Optional) Debug messages and warnings
            %           (Default: true).
            % OUTPUT skel: A Skeleton object.
            %
            % NOTE nodeOffset should be set to 1 when reading or writing
            %      files made in webknossos due to the webknossos node
            %      offset.
            options.dummy = [];
            if nargin > 0
                if ~exist('justCloneParameters','var') || isempty(justCloneParameters)
                    justCloneParameters = false;
                end
                if exist('verbose','var') && ~isempty(verbose)
                    obj.verbose = verbose;
                end
                if ~exist('nodeOffset','var') || isempty(nodeOffset)
                    nodeOffset = 1;
                elseif length(nodeOffset) ~= 1
                    error('NodeOffset must be a scalar.');
                end

                obj.nodeOffset = nodeOffset;
                if nodeOffset ~= 1 && obj.verbose
                    warning('Node offset is set to %d.',nodeOffset);
                end
                [~,obj.filename, ending] = fileparts(filename);
                if ~strcmpi(ending, '.nml')
                    filename = [filename '.nml'];
                end
                if isfield(options, 'useCurrentParseNml') && options.useCurrentParseNml
                    if obj.verbose
                        temp = parseNml(filename, 1, 0, 1, nodeOffset);
                    else
                        [~, temp] = evalc('parseNml(filename, 1, 0, 1, nodeOffset)');
                    end
                else
                    if obj.verbose
                        temp = parseNmlv027(filename, 1, 0, 1, nodeOffset);
                    else
                        [~, temp] = evalc('parseNmlv027(filename, 1, 0, 1, nodeOffset)');
                    end
                end
                if justCloneParameters
                    obj.parameters=temp{1}.parameters;
                    obj.scale = structfun(@str2double, ...
                        obj.parameters.scale)';
                    return;
                end
                obj.thingIDs = zeros(length(temp),1);
                obj.nodes = cell(length(temp),1);
                obj.edges = cell(length(temp),1);
                obj.names = cell(length(temp),1);
                obj.colors = cell(length(temp),1);
                obj.nodesAsStruct = cell(length(temp),1);
                obj.nodesNumDataAll = cell(length(temp),1);
                tmax = zeros(length(temp),1);
                for i=1:length(temp)
                    obj.nodes{i} = temp{i}.nodes;
                    obj.edges{i} = temp{i}.edges;
                    obj.thingIDs(i) = temp{i}.thingID;
                    obj.names{i} = temp{i}.name;
                    obj.nodesAsStruct{i} = temp{i}.nodesAsStruct;
                    obj.nodesNumDataAll{i} = temp{i}.nodesNumDataAll;
                    if isfield(options, 'useCurrentParseNml') && options.useCurrentParseNml
                        obj.nodesTable{i} = array2table(temp{i}.nodesNumDataAll, 'VariableNames', {'id', 'radius', 'x', 'y', 'z', 'inVp', 'inMag', 'rotX', 'rotY', 'rotZ', 'bitDepth', 'interpolation', 'withSpeed', 'time'});
                    end
                    %handle empty trees (will produce warning below)
                    if ~isempty(temp{i}.nodesNumDataAll)
                        tmax(i) = max(temp{i}.nodesNumDataAll(:,1));
                    end
                end
                if any(tmax == 0)
                    warning('Nml file %s contains empty trees.', filename);
                end
                obj.largestID = max(tmax);
                obj.parameters = temp{1}.parameters;
                obj.branchpoints = temp{1}.branchpoints;
                obj.scale = structfun(@str2double,obj.parameters.scale)';
            else %return empty Skeleton object
                obj.thingIDs = [];
                obj.nodes = {};
                obj.edges = {};
                obj.names = {};
                obj.colors = {};
                obj.nodesAsStruct = {};
                obj.nodesNumDataAll = {};
                obj.largestID = 0;
                obj.parameters = struct;
                obj.branchpoints = [];
                obj.scale = [];
                obj.nodeOffset = 1;
                obj.verbose = 0;
            end
        end
    end

    methods (Static)
        l = physicalPathLength(nodes, edges, voxelSize)
        [skel, treeOrigin] = loadSkelCollection(paths, nodeOffset, ...
            toCellOutput)
        skel = fromCellArray(c);
        skel = loadSkelCollectionFromSubfolders(paths, nodeOffset, ...
            toCellOutput)
    end
end
