function c = struct2nmcell( s )
%STRUCT2NMCELL Convert a struct into an array of name-value pairs with the
%fieldnames as names.
% INPUT s: A scalar struct.
% OUTPUT c: Cell array of size [1x2N], where N is the number of fields of
%           s. c iterates between a fieldname of s and then the respective
%           value.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

names = fieldnames(s);
values = struct2cell(s);
c = reshape([names, values]',1, 2*length(names));
end

