function [ Y, mapping ] = renumber( X, mode )
%RENUMBER Renumber the unique elements in X as integers starting from 1.
% INPUT X: Numeric array
%       mode: (Optional) String specifying renumber mode
%           'stable': IDs are renumbered according to their occurrence in
%                     X(:).
%           'sorted': (Default) IDs are renumbered according to occurence
%                     in unique(X(:));
% OUTPUT Y: Renumbered array of the same size as X.
%        mapping: [Nx2] array containing the inverse mapping of renumber.
%           Use renumberBwd with Y and mapping to get X.
%
% NOTE To apply the same renumbering to another array X2 the following code
%      can be used
%      fwdMap = zeros(max(mapping(:,1)), 1);
%      fwdMap(mapping(:,1)) = mapping(:,2);
%      X2 = fwdMap(X2);
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

sX = size(X);
X = X(:);
if ~exist('mode','var') || isempty(mode)
    mode = 'sorted';
end
switch mode
    case 'stable'
        [~,ix,ic] = unique(X,'stable');
    case 'sorted'
        [~,ix,ic] = unique(X);
    otherwise
        error('Unknown mode %s.',mode);
end

Y = cast(ic,'like',X);

if nargout == 2
    mapping = [X(ix),Y(ix)];
end

Y = reshape(Y,sX);


end

