function skel = removeTPAWithComment( skel, comment, mode )
%REMOVETPAWITHCOMMENT Remove three-point annotations with the specified
% comment.
% INPUT comment: string
%           see skeleton.getNodesWithComment
%       mode: string
%           see skeleton.getNodesWithComment
% OUTPUT skel: skeleton object
%           Skeleton object with TPAs removed.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

nodes = skel.getNodesWithComment(comment, [], mode);

if ~iscell(nodes)
    nodes = {nodes};
end

for tr = 1:skel.numTrees()
    toDel = find(skel.reachableNodes(tr, nodes{tr}, 1, 'up_to'));
    skel = skel.deleteNodes(tr, toDel); %#ok<FNDSB>
end

end

