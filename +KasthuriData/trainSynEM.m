%% script for training SynEM on the kat11 data
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% set this to the folder downloaded from the synEM website
pipelineFolder = ['/media/benedikt/DATA2/workspace/data/backed/' ...
    'Synapse detection/KasthuriData/kat11/'];

% load pipeline parameter struct
m = load(fullfile(pipelineFolder, 'allParameter.mat'));
p = m.p;
p = SynEM.Seg.Util.changePaths(p, pipelineFolder);


%% features

% define the feature map
voxelSize = [6 6 30];
fm = SynEM.getFeatureMap('paper', voxelSize);
fm.setSelectedFeat(true(fm.numFeatures, 1));

% load the precalculated features
X = SynEM.Seg.Global.getInterfaceFeatures(p, 1:150444, [], false, ...
    'InterfaceFeatures.mat', fm);


%% definition of training and test indices

m = load(fullfile(p.saveFolder, 'globalBorder.mat'));
borderCoM = m.borderCoM;
borderSize = m.borderSize;

%testing in ac3
bbox_ac3 = [5472, 6496; 8712, 9736; 1000, 1256];

% leave margin around ac3 of 1 um
margin = [166; 166; 33];
bboxMargin = bsxfun(@plus, bbox_ac3, [-margin, margin]);

% get interface idx in the cubes
[exclTrainIdx, ~, testIdx] = KasthuriData.splitData(borderCoM, ...
    bboxMargin, [], bbox_ac3);

% use everything outside of margin for training
trainIdx = ~exclTrainIdx;

assert(~any(trainIdx & testIdx));

% further discard training indices between edges with the same ids
% as in the test set within 2 um of the test set bbox
%(just to make sure - I actually checked the close synapses and nothing is
%overlapping)
m = load(fullfile(p.saveFolder, 'globalEdges.mat'));
idx = Graph.findEdges(m.edges(trainIdx, :),  m.edges(testIdx,:));
idx(cellfun(@isempty,idx)) = [];
idx = unique(cell2mat(idx));
tmp = false(sum(trainIdx), 1);
tmp(idx) = true;
idx = tmp;
tmp = false(size(m.edges, 1), 1);
tmp(trainIdx) = idx;
idx = tmp;
d = KasthuriData.distToBbox(bbox_ac3, borderCoM(idx,:), voxelSize);
idx(idx) = d < 2000;
trainIdx(idx) = false;

% discard small interfaces
idx_areaT = borderSize > 150;


%% get training and test features & labels

% labels
mLabel = load(fullfile(p.saveFolder, 'synapseDirectionGT_v3.mat'));
exclList = mLabel.exclList;
X_train = X(trainIdx & idx_areaT & ~exclList, :);
X_test = X(testIdx & idx_areaT & ~exclList, :);
y_train = mLabel.label_hyb(trainIdx & idx_areaT & ~exclList);
y_test = mLabel.label_hyb(testIdx & idx_areaT & ~exclList);
group_test = mLabel.group_hyb(testIdx & idx_areaT & ~exclList);
if length(unique(group_test)) ~= length(unique(mLabel.group_hyb(testIdx)))
    warning('Synapses got lost via the area threshold or exclusion.');
end
assert(~any(isnan(X_train(:))));
assert(~any(isnan(X_test(:))));


%% train the classifier

% direction labels
X_train = cat(1, X_train, fm.invertDirection(X_train));
y_train = [y_train == 1; y_train == 2];

% classifier training
classifier = SynEM.Classifier.BoostedEnsemble.train(X_train, y_train);

% classifier is stored at 'KasthuriDirClassifier.mat' in the pipeline
% folder


%% evaluate the classifier

X_test = cat(1, X_test, fm.invertDirection(X_test));
[~, scores] = classifier.predict(X_test);
scores = reshape(scores, [], 2);
scores = max(scores, [], 2); % maximum over both directions

% synapse detection (grouping) performance
[rp, thresholds] = SynEM.Eval.interfaceRP_v2(group_test, scores);

