function segIds = getSegmentIdsOfNodes(param, coords)
    % GETSEGMENTIDS
    %   Gets the global segment IDs for each of the points
    %   specified in coords.
    %
    % IMPORTANT
    %   In contrast to getSkelSegmentIDs, this function is
    %   ready to be used with the pipeline repository!
    %
    % param
    %   Parameter structure thath was produced with
    %   run configuration.m
    %
    % coords
    %   Nx3 matrix of doubles. Each row corresponds to a
    %   position vector in global coordinates.
    %
    % segIds
    %   Nx1 vector of global segment IDs. The entries are
    %   zero if the point lies on a border and -1 if the
    %   point is outside the segmented bounding box.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    box = param.bbox;
    
    % build mask for valid coords
    mask =  ...
        all(bsxfun(@ge, coords, box(:, 1)'), 2) ...
      & all(bsxfun(@le, coords, box(:, 2)'), 2);
    
    % prepare output
    coordCount = size(coords, 1);
    segIds = nan(coordCount, 1);
    
    % lookup segment IDs for valid coordinates
    segIds(mask) = SynEM.Seg.Global.getSegIds(param, coords(mask, :));
    
    % set invalid coordinates to minus one
    segIds(~mask) = -1;
end