% labels from nml files and kat synapse segmentation
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% set this to the folder downloaded from the synEM website
pipelineFolder = ['/media/benedikt/DATA2/workspace/data/backed/' ...
    'Synapse detection/KasthuriData/kat11/'];

% load pipeline parameter struct (warnings due to function handles can be
% ignored)
m = load(fullfile(pipelineFolder, 'allParameter.mat'));
p = m.p;
p = SynEM.Seg.Util.changePaths(p, pipelineFolder);

p.seg.root = fullfile(pipelineFolder, ['globalSeg', filesep]);


%% load labels

% kd_sj = KnossosDataset(saveFolderSynapses); % see downloadDataScript
% KasthuriData.getSynapticBorders(p, kd_sj, 'overlap_at', 10);

m = load(fullfile(p.saveFolder, 'globalEdges.mat'));
edges = m.edges;

m = load(fullfile(p.saveFolder, 'globalBorder.mat'));
borderCoM = m.borderCoM;

% load the interface labels from wk tracing
% % direct calculation via the skeleton file:
% skel = skeleton(fullfile(pipelineFolder, 'SynapseDirectionLabels_v3.nml'));
% skel = skel.sortTreesById();
% segIds = Skeleton.getSegmentIdsOfSkel(p, skel);
% if ~exist(fullfile(pipelineFolder, 'SynapseDirectionLabels_v3.mat'), 'file')
%     save(fullfile(pipelineFolder, 'SynapseDirectionLabels_v3.mat'), ...
%         'skel', 'segIds');
% end
% load from precalculated file:
m = load(fullfile(pipelineFolder, 'SynapseDirectionLabels_v3.mat'));

% get all synapses (including unsure)
[label_all, group_all] = KasthuriData.getDirectionLabels(m.segIds, ...
    m.skel, edges, borderCoM);

% combine with gt segmentation
[label_all_hyb, group_all_hyb] = KasthuriData.hybridLabels(p, ...
    label_all, group_all);

% get synapses without unsure labels
unsureLabels = [9, 10];
[label, group, exclList] = KasthuriData.getDirectionLabels(m.segIds, ...
    m.skel, edges, borderCoM, ismember(m.synLabel, unsureLabels));

% combine with gt segmentation
[label_hyb, group_hyb] = KasthuriData.hybridLabels(p, label, group);


%% save result

outfile = fullfile(p.saveFolder, 'synapseDirectionGT_v3.mat');
if ~exist(outfile, 'file')
    save(outfile, 'label', 'group', ...
        'exclList', 'label_hyb', 'group_hyb', 'info', ...
        'label_all', 'group_all', 'label_all_hyb', 'group_all_hyb');
end