function [bbox, info] = toKnossosDataset( kd, token, bbox, res )
%TOKNOSSOSDATASET Download the data and write it to a knossos dataset.
% INPUT kd: KnossosDataset object or struct
%           The knossos dataset object or parameter struct.
%       token: string
%           Dataset token. 'kasthuri11cc' and 'kasthuri11' will be
%           automatically set as image token and all others as anno token.
%       bbox: (Optional) [3x2] int
%           The bounding box to download. (Python style, i.e. from start to
%           last - 1). Also note that this is the bbox in the kasthuri data
%           and the matlab bbox will be different be different such that it
%           starts from 1 (i.e. using the offset [1;1;0]).
%       res: (Optional) int
%           The resolution for the data.
%           (Default: 1 corresponding to 8 x 8 x 40 nm voxel size).
% OUTPUT bbox: [3x2] int
%           The bounding box in the knossos dataset (aligned to wk cubes).
%        info: struct
%           Information about the dataset.
%
% NOTE The kd bounding box will be shifted by -(offset - 1).
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if isstruct(kd)
    kd = KnossosDataset(kd);
end
Util.log('Saving data to %s.', kd.root);

if ~exist('res', 'var') || isempty(res)
    res = 1;
end

% prepare ocp db service
cajal3d;
oo = OCP();
oo.setServerLocation('http://openconnecto.me');

if ~isempty(strfind(token, 'kasthuri11'))
    oo.setImageToken(token);
    q = OCPQuery;
    q.setType(eOCPQueryType.imageDense);
    im_size = oo.imageInfo.DATASET.IMAGE_SIZE(res);
    offset = oo.imageInfo.DATASET.OFFSET(res);
    info = oo.imageInfo;
else
    oo.setAnnoToken(token);
    q = OCPQuery;
    q.setType(eOCPQueryType.annoDense);
    im_size = oo.annoInfo.DATASET.IMAGE_SIZE(res);
    offset = oo.annoInfo.DATASET.OFFSET(res);
    info = oo.annoInfo;
end
offset = offset(:);
im_size = im_size(:);

if ~exist('bbox', 'var') || isempty(bbox)
    bbox = [offset, im_size + offset - 1];
end

% set upper end of bbox to maximal image size
bbox(:,2) = min(bbox(:,2), im_size + offset - 1);

%iterate over kd cubesizes
cubesize = kd.cubesize(1:3);
numCubes = length(bbox(1,1):cubesize(1):(bbox(1,2) - 1)) * ...
           length(bbox(2,1):cubesize(2):(bbox(2,2) - 1)) * ...
           length(bbox(3,1):cubesize(3):(bbox(3,2) - 1));
count = 1;
Util.log('Loading data ''%s'' for bbox %s.', token, mat2str(bbox));
tic
for x = bbox(1,1):cubesize(1):(bbox(1,2) - 1)
    for y = bbox(2,1):cubesize(2):(bbox(2,2) -1)
        for z = bbox(3,1):cubesize(3):(bbox(3,2) - 1)
            this_bbox = [x, min(x + cubesize(1), bbox(1,2)); ...
                         y, min(y + cubesize(2), bbox(2,2)); ...
                         z, min(z + cubesize(3), bbox(3,2))];
            
            q.setCutoutArgs(this_bbox(1,:), this_bbox(2,:), ...
                this_bbox(3,:), res);
            file = oo.query(q);
            data = file.data;
            warning('off', 'all')
            
            % write kd cubes (x and y are interchanged here to have same
            % coordinates as on openconnectome viewer)
            data = permute(data, [2 1 3]);
            kd.writeRoi(this_bbox(:,1) - (offset - 1), data);
            if mod(count, 100) == 0
                Util.log('Finished %d/%d cubes.', count, numCubes);
            end
            count = count + 1;
        end
    end
end
Util.log('Finished %d cubes in %.2f seconds', numCubes, toc);
warning('on', 'all')

bbox = bsxfun(@plus, bbox, 1 - offset);

end

