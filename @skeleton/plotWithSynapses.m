function plotWithSynapses( skel, treeIndices, syNameCell, umscale, varargin)
%PLOTWITHSYNAPSES Simple line plot of Skeleton plus synapses as scatter
% balls with the legend displaying their identity 
% INPUT treeIndices: (Optional) [1*N] vector of linear indices
%                of trees to check for.
%                (Default: all trees)
%       syNameCell: {1*N} cell with the names of the synapse labels used
%           even partially, excluding the synapses having 'unsure' label
%           savingDir
%       umscale: (Optional, added by AK 20.02.2017) logical if set true
%           sets the scale to micrometer 
%           (Default: nanometer scale)
%       varargin: 
%          It can include 'cmap': colormap for synapse type, 'angle': The
%          viewing angle of, 'showLegend': logical to see the legend,
%          'showAxis': Logical to show axis or not,getSynCenterBackBone: if
%          set to true the center of synapse is chosen and the tree is
%          trimmed to its backbone. Example function call:

%           skel.plotWithSynapses(i,{'sh','sp'},true,'angle',[0 -90]...
%            ,'showLegend',false,'showAxis',false,'getSynCenterBackBone',true);

% Author: Ali Karimi <ali.karimi@brain.mpg.de>

% This piece of code parses the varargin based on: 
%https://de.mathworks.com/matlabcentral/answers/127878-interpreting-varargin-name-value-pairs

p=inputParser;
defaultCmap=colormap(lines);
defaultangle=[0,0];
defaultLegend=true;
defaultAxis=true;
defaultgetSynCenter=false;
defaultgetBackBone=false;
%create parameters setting them to default
addParameter(p,'cmap',defaultCmap);
addParameter(p,'angle',defaultangle);
addParameter(p,'showLegend',defaultLegend,@islogical);
addParameter(p,'showAxis',defaultAxis,@islogical);
addParameter(p,'getSynCenter',defaultgetSynCenter,@islogical);
addParameter(p,'getBackBone',defaultgetBackBone,@islogical);
% Parsing varargin and setting the values
parse(p,varargin{:});
cmap=p.Results.cmap;
angle=p.Results.angle;
showLegend=p.Results.showLegend;
showAxis=p.Results.showAxis;
getSynCenter=p.Results.getSynCenter;
getBackBone=p.Results.getBackBone;


if ~exist('treeIndices','var') || isempty(treeIndices)
    treeIndices = 1:skel.numTrees();
end

if ~exist('umscale','var') || isempty(umscale) || umscale == 0
    scale = skel.scale;
    umscale = 0;
else
    scale =skel.scale/1000;
end
if ~exist('syNameCell','var') || isempty(syNameCell)
    syNameCell = 'syn';
end
if ischar(syNameCell)
    syNameCell = {syNameCell};
end
% give each tree a unique color
c = lines(size(treeIndices,2));
% use a counter to set the tree color from the lines color map
treeCounter=1;
for tr = treeIndices(:)'
    %coordinate of all Nodes
    trNodes = bsxfun(@times,skel.nodes{tr}(:,1:3),scale);
    % Let's plot the tree here first and trim it to the backBone
    if getBackBone
        % Specific code for AK, you need to adjust this for your own commenting scheme
        % The tree color: Black for both cases currently
        if ~isempty(strfind(skel.names{tr},'cell'))
        skel.plot(tr,[0 0 0],umscale,2,{'exit','start',syNameCell{2}});
        else
        skel.plot(tr,[0 0 0],umscale,2,{'exit','start',syNameCell{2}});
        end
    else
        skel.plot(tr,c(treeCounter,:),umscale);
    end
    %Get all the synapses which have unsure and ignore string in their
    %names
    unsureSy = skel.getNodesWithComment('unsure', tr, 'partial');
    ignoreSy = skel.getNodesWithComment('unverified', tr, 'partial');
    %Plot each s
    for sy = 1:size(syNameCell,2)
        allSynapses = skel.getNodesWithComment(syNameCell{sy},tr,'partial');
        synapses = setdiff(allSynapses,unsureSy);
        synapses = setdiff(synapses,ignoreSy);
        if getSynCenter
        synapses=skel.getSynCenterFromTPA(tr,synapses);
        end
        synCoords = trNodes(synapses,:);
        s(sy) = scatter3(synCoords(:,1),synCoords(:,2),synCoords(:,3),120,...
            'filled', 'MarkerEdgeColor', cmap(sy,:), ...
            'MarkerFaceColor', cmap(sy,:));
        s(sy).DisplayName = syNameCell{sy};
    end
    treeCounter=treeCounter+1;
end
%Display the legend
if showLegend
legend(s,'Location','northeast');
end

%Set the correct aspect ratio
daspect([1 1 1])
%Set the viewing angle
view(angle);
if size(treeIndices,2)==2
    title(regexprep(skel.names(treeIndices),'_','\\_'));
end
if showAxis
    %Add labels to axis
    if umscale
        xlabel('x (\mum)')
        ylabel('y (\mum)')
        zlabel('z (\mum)')
    else
        xlabel('x (nm)')
        ylabel('y (nm)')
        zlabel('z (nm)')
    end
else
    axis off;
end
end

