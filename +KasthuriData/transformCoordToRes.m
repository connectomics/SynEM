function [ coords ] = transformCoordToRes( fromRes, toRes, coords )
%TRANSFORMCOORDTORES Transform coordinates to desired resolution.
% INPUT fromRes: int
%           Resolution of the input coordinates.
%       toRes: int
%           Resolution for the output coordinates.
%       coords: [Nx3] int
%           Coordinates in fromRes.
% OUTPUT coords: [Nx3] int
%           Coordinates in toRes.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

coords(:,[1 2]) = coords(:,[1 2]).*2^(fromRes - toRes);
coords = round(coords);

end

