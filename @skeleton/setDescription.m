function skel = setDescription( skel, descr )
%SETDESCRIPTION Set the tracing description.
% INPUT descr: string
%           The description text. The description can contain %s to
%           substitute the old description.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~isfield(skel.parameters.experiment, 'description')
    skel.parameters.experiment.description = '';
end

skel.parameters.experiment.description = sprintf(descr, ...
    skel.parameters.experiment.description);

end

