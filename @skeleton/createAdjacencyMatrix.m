function am=createAdjacencyMatrix(obj,treeIdx)
%Create the adjacency matrix of a tree of the Skeleton.
% INPUT treeIdx: Integer index of the tree in obj to create
%           the adjacency matrix for.
% OUTPUT am: Sparse, rectangular and symmatric matrix,
%           where am(i,j) = am(j,i) =  1, if node i and node j
%           are connected.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

numNodes = size(obj.nodes{treeIdx}, 1);

if numNodes > 1
    edgesInTree = obj.edges{treeIdx};
    edgeCount = size(edgesInTree, 1);
    
    % build adjacency matrix
    am = sparse( ...
        edgesInTree(:, 1)', edgesInTree(:, 2)', ...
        true(1, edgeCount), numNodes, numNodes);
    
    % make symmetric
    am = am + am';
else
    % single node case
    am = zeros(1);
end
end