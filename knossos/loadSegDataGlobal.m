function seg = loadSegDataGlobal(param, bbox)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    seg = readKnossosRoi( ...
        param.root, param.prefix, bbox, 'uint32');
end

