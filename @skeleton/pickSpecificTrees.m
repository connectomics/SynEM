function  treesToPick=pickSpecificTrees(obj, names,mode, outputNml)
%Choose specific trees to
% INPUT treeNames: 1xN Cell array with the name of trees to be deleted
%       outputNml: The name of nml with the specific trees to be writted
%       mode: search mode for the tree name, check description of
%       getTreeWithName method
% OUTPUT treesToPick: the id of the trees that are written into the next
% skeleton
% Author: Ali Karimi <ali.karimi@brain.mpg.de>

treesToPick=[];

for i=1:length(names)
    treesToPick=[treesToPick,obj.getTreeWithName(names{i},mode)];
end
assert(length(names)==length(treesToPick),'Tree missing or too many trees')
obj.write(outputNml,treesToPick);

end
