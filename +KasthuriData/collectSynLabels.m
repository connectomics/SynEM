function synLabel = collectSynLabels( p, synGTFile )
%COLLECTSYNLABELS Collect the synapse labels from each local segmentation
%cube.
% INPUT p: struct
%           Segmentation parameter struct.
%       synGTFile: (Optional) string
%           Filename of the synapse ground truth file. The file should
%           contain a variable synId with the synapse labels. The output
%           will simply be synIds > 0.
%           (Default: 'synapseGT.mat')
% OUTPUT synLabel: [Nx1] logical
%           Interface labels. Synapses are labeled with true.
%
% see also KasthuriData.getSynapticBorder
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('synGTFile', 'var') || isempty(synGTFile)
    synGTFile = 'synapseGT.mat';
end

synLabel = cell(numel(p.local), 1);
for i = 1:numel(p.local)
    m = load(fullfile(p.local(i).saveFolder, synGTFile));
    synLabel{i} = m.synId > 0;
end
synLabel = cell2mat(synLabel);


end

