function [label, group] = hybridLabels(p, label, group)
%HYBRIDLABELS Combine the direction labels with border overlap labels.
% The direcion labels are only considered with borders that also overlap
% with the PSD segmentation.
% INPUT p: struct
%           Kasthuri data parameter struct.
%       label: [Nx1] int
%           The interface direction labels.
%           (see first output of KasthuriData.getDirectionLabels)
%       group: [Nx1] int
%           The interface grouping.
%           (see second output of KasthuriData.getDirectionLabels)
% OUTPUT label: [Nx1] int
%           The updated labels.
%        group: [Nx1] int
%           The updated groups.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% load the overlap labels from all fileparts
overlapLabels = KasthuriData.collectSynLabels(p, 'synapseGT_at.mat');

assert(length(overlapLabels) == length(label));

% intersect both labels
label(~overlapLabels) = 0;
group(~overlapLabels) = 0;
group(group > 0) = Util.renumber(group(group > 0));
end
