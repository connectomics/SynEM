%% script to download the kasthuri data
% requires cajal in path
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% note /1 here means res = 1, but kasthuri res starts from 0
root = '/home/benedikt/data/kat11/';
saveFolderRaw = [root 'color/1' filesep];
saveFolderSeg = [root 'seg_orig/1' filesep];
saveFolderSegAdapt = [root 'segmentation/1' filesep];
saveFolderVesicles = [root 'vesicles/1' filesep];
saveFolderMitos = [root 'mito/1' filesep];
saveFolderSynapses = [root 'synapses/1' filesep];

%% download the raw data in res 1 with 3 cylinder bbox which is
% bounding box at res 3: X: [694,1794]; Y: [1750,2460]; Z: [1004,1379]
% see https://github.com/neurodata/kasthuri2015/blob/master/claims/claim0_get_data.ipynb

kd_raw = KnossosDataset(saveFolderRaw, 'kat11');
kd_seg = KnossosDataset(saveFolderSeg, 'kat11', 'uint32');

% above bbox *4 aligned with lower knossos cubes
bbox_res1 = [2776 7176; 7000 9840; 1004 1379];
bbox_res1(:,1) = bbox_res1(:,1) - mod(bbox_res1(:,1), 128) + [0; 0; 1]; %last term due to offset
bbox_res1(:,2) = bbox_res1(:,2) + 128 - mod(bbox_res1(:,2), 128) - [1; 1; 0];

% add two knossos cubes of border (xy) and one in (z)
bbox_res1 = bsxfun(@plus, bbox_res1, [-[256; 256; 128], [257; 257; 129]]);

[bbox_kd, info_raw] = KasthuriData.toKnossosDataset(kd_raw, ...
    'kasthuri11cc', bbox_res1, 1);
[~, info_seg] = KasthuriData.toKnossosDataset(kd_seg, 'kat11segments', ...
    bbox_res1, 1);

%% download other ultrastructure labels

kd_v = KnossosDataset(saveFolderVesicles, 'kat11', 'uint32');
kd_mi = KnossosDataset(saveFolderMitos, 'kat11', 'uint32');
kd_sj = KnossosDataset(saveFolderSynapses, 'kat11', 'uint32');

[~, info_ves] = KasthuriData.toKnossosDataset(kd_v, 'kat11vesicles', ...
    bbox_res1, 1);
[~, info_mito] = KasthuriData.toKnossosDataset(kd_mi, 'kat11mito', ...
    bbox_res1, 1);
[~, info_syn] = KasthuriData.toKnossosDataset(kd_sj, 'kat11synapses', ...
    bbox_res1, 1);

%% save info files

save(fullfile(root, 'info.mat'), 'info_raw', 'info_seg', 'info_ves', ...
    'info_mito', 'info_syn');

%% adapt the segmentation to have one voxel boundaries

kd_seg_ad = KnossosDataset(saveFolderSegAdapt, 'kat11', 'uint32');
KasthuriData.adaptKDSegmentation(kd_seg, kd_seg_ad, bbox_kd, [1024 1024 512]);

%% lower resolutions (only needed to view higher resolutions in webknossos)

bbox_res = cell(7, 1);
bbox_res{1} = bbox_res1;
for i = 2:7
    bbox_res{i} = bbox_res1;
    bbox_res{i}(1:2,:) = round(bbox_res{i}(1:2,:)./(2^(i - 1)));
    kd_raw.switchMag(i);
    kd_seg.switchMag(i);
    kd_v.switchMag(i);
    kd_mi.switchMag(i);
    kd_sj.switchMag(i);
    KasthuriData.toKnossosDataset(kd_raw, 'kasthuri11cc', bbox_res{i}, i);
    KasthuriData.toKnossosDataset(kd_seg, 'kat11segments', bbox_res{i}, i);
    KasthuriData.toKnossosDataset(kd_v, 'kat11vesicles', bbox_res{i}, i);
    KasthuriData.toKnossosDataset(kd_mi, 'kat11mito', bbox_res{i}, i);
    KasthuriData.toKnossosDataset(kd_sj, 'kat11synapses', bbox_res{i}, i);
end

%% synapse centroids

m = load(fullfile(root, 'allParameter.mat'));
p = m.p;
kd_sj = KnossosDataset(saveFolderSynapses, [], 'uint32');
synSeg = kd_sj.readRoi(p.bbox);
stats = regionprops(synSeg > 0, synSeg, 'Centroid', 'PixelIdxList', ...
    'BoundingBox', 'MaxIntensity');
stats2 = regionprops(synSeg > 0, synSeg, 'Centroid', 'PixelIdxList', ...
    'BoundingBox', 'MinIntensity');
stats = stats(:);
synCen = {stats.Centroid};
synCen = round(cell2mat(synCen'));
synCen = synCen(:, [2 1 3]);
synCen = bsxfun(@plus, synCen, p.bbox(:,1)' - 1);
psdArea = SynEM.Seg.Local.physicalBorderArea(stats, [6 6 30], ...
    size(synSeg), 1e-5);
isAtBorder = cellfun(@(x) any(x(1:3) < 1) | ...
    any((x(1:3) + x(4:6)) > (size(synSeg) - 0.5)), {stats.BoundingBox});
p.seg.root = kd_sj.root;
synLabel = Skeleton.getSegmentIdsOfNodes(p, synCen);
save(fullfile(root, 'kat11_synapses.mat'), 'synCen', ...
    'psdArea', 'isAtBorder', 'synLabel');

% max and min that are inspected by hand
synLabelMax = [stats.MaxIntensity]';
synLabelMin = [stats2.MinIntensity]';