function skelDeleted=getBackBone(skel,tr,comments2Avoid)
% getBackBone This function deletes all the branch nodes until there's no
% degree 1 node left except for the real endings specified by
% comments2Avoid
%   INPUT:
%          tr: The tree of Interest
%           comments2Avoid: 1xN  cell with comments specifying the real
%           tree endings.add your spine comment to keep the spine neck such
%           as 'sp' or 'Spine', in case comment sp eists last node is
%           deleted to get rid of the last edge of TPA
%        
% 	OutPut:
%           skelDeleted: The skeleton class with tree=tr trimmed to
%           backbone
%   Authors: Ali Karimi <ali.karimi@brain.mpg.de>
%            Jan Odenthal <jan.odenthal@brain.mpg.de>

realTreeEndings=[];
for i=1:size(comments2Avoid,2)
    realTreeEndings=cat(1,realTreeEndings,skel.getNodesWithComment(comments2Avoid{i},tr,'partial'));
end
unsureSy = skel.getNodesWithComment('unsure', tr, 'partial');
ignoreSy = skel.getNodesWithComment('unverified', tr, 'partial');
realTreeEndings=setdiff(realTreeEndings,union(unsureSy,ignoreSy));

%Init the skeleton with trimmed tree
skelDeleted=skel;
%Get the first round of nodes which need to be deleted
nodes2Del=skelDeleted.getBranchNodesToDel(tr,realTreeEndings);
%This while loop would continue until there's no
while ~isempty(nodes2Del)
    %Delete the nodes from last round
    skelDeleted=skelDeleted.deleteNodes(tr,nodes2Del,true);
    %Update the real tree endings
    realTreeEndings=[];
    nodes2Del=[];
    endings2Keep=[];
    for i=1:size(comments2Avoid,2)
        realTreeEndings=cat(1,realTreeEndings,skelDeleted.getNodesWithComment(comments2Avoid{i},tr,'partial'));
    end
    unsureSy = skelDeleted.getNodesWithComment('unsure', tr, 'partial');
    ignoreSy = skelDeleted.getNodesWithComment('unverified', tr, 'partial');
    realTreeEndings=setdiff(realTreeEndings,union(unsureSy,ignoreSy));
    nodes2Del=skelDeleted.getBranchNodesToDel(tr,realTreeEndings);
end
%if spines are avoided delete the last extra node of the spine 3 point
%annotation
if any(cellfun(@(x)~isempty(strfind(x,'sp')),comments2Avoid))
spineLastNode=skelDeleted.getNodesWithComment('sp',tr,'partial');
skelDeleted=skelDeleted.deleteNodes(tr,spineLastNode,true);
end
end





