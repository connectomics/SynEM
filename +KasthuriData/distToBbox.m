function [ d ] = distToBbox( bbox, points, scale )
%DISTTOBBOX Calculate the euclidean distance of points to a bounding box.
% INPUT bbox: [3x2] double
%           The bounding box
%       points: [Nx3] double
%           The coordinates for which the distance is calculated.
%       scale: (Optional) [3x1] double
%           Scaling factor for each coordinate.
%           (Default: ones(3, 1))
% OUTPUT d: [Nx1] double
%           The distance of each points to the bbox.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if exist('scale', 'var') && ~isempty(scale)
    points = bsxfun(@times, double(points), scale(:)');
    bbox = bsxfun(@times, double(bbox), scale(:));
else
    points = double(points);
    bbox = double(bbox);
end

           
below = bsxfun(@le, points, bbox(:,1)');
beyond = bsxfun(@ge, points, bbox(:,2)');

d_below = bsxfun(@minus, points, bbox(:,1)').^2;
d_beyond = bsxfun(@minus, points, bbox(:,2)').^2;

d_below(~below) = 0;
d_beyond(~beyond) = 0;
d = d_below + d_beyond;
d = sqrt(sum(d, 2));

end

